package com.morpheus.redis.jedis.cluster;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClusterConnection;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

public class JedisClusterImportSelector implements ImportSelector {
	private static final Logger LOGGER = LoggerFactory.getLogger(JedisClusterImportSelector.class);

	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		String[] beanNames = new String[] { JedisPoolConfig.class.getName(), JedisCluster.class.getName(),
				JedisClusterConnection.class.getName(), RedisClusterConfiguration.class.getName(),
				JedisConnectionFactory.class.getName() };
		LOGGER.debug("JedisClusterImportSelector.selectImports() beanNames={}", new Object[] { beanNames });
		return beanNames;
	}
}
