package com.morpheus.redis.jedis.cluster;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class OnEnableJedisClusterCondition extends SpringBootCondition {
	private static final Logger LOGGER = LoggerFactory.getLogger(OnEnableJedisClusterCondition.class);

	@Override
	public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {
		Environment environment = context.getEnvironment();
		boolean isCluster = environment.containsProperty("spring.redis.cluster.nodes")
				|| environment.containsProperty("spring.redis.cluster.maxRedirects");
		boolean isJedis = environment.containsProperty("spring.redis.jedis.pool.max-idle")
				|| environment.containsProperty("spring.redis.jedis.pool.min-idle")
				|| environment.containsProperty("spring.redis.jedis.pool.max-active")
				|| environment.containsProperty("spring.redis.jedis.pool.max-wait");
		ConditionOutcome conditionOutcome = new ConditionOutcome(isCluster && isJedis,
				"Enable Redis-Cluster client Jedis.");
		LOGGER.debug("OnEnableJedisClusterCondition.getMatchOutcome() conditionOutcome={}", conditionOutcome);
		return conditionOutcome;
	}
}
