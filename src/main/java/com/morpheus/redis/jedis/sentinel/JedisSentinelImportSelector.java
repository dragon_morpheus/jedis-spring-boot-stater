package com.morpheus.redis.jedis.sentinel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisSentinelPool;

public class JedisSentinelImportSelector implements ImportSelector {
	private static final Logger LOGGER = LoggerFactory.getLogger(JedisSentinelImportSelector.class);

	public JedisSentinelImportSelector() {
	}

	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		String[] beanNames = new String[] { JedisPoolConfig.class.getName(), JedisSentinelPool.class.getName(),
				JedisConnectionFactory.class.getName(), RedisSentinelConfiguration.class.getName() };
		LOGGER.debug("JedisSentinelImportSelector.selectImports() beanNames={}", new Object[] { beanNames });
		return beanNames;
	}
}
