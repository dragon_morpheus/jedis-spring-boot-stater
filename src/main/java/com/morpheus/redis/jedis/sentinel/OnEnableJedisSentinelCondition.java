package com.morpheus.redis.jedis.sentinel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class OnEnableJedisSentinelCondition extends SpringBootCondition {
	private static final Logger LOGGER = LoggerFactory.getLogger(OnEnableJedisSentinelCondition.class);

	@Override
	public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {
		Environment environment = context.getEnvironment();
		boolean isSentinel = environment.containsProperty("spring.redis.sentinel.master")
				|| environment.containsProperty("spring.redis.sentinel.nodes");
		boolean isJedis = environment.containsProperty("spring.redis.jedis.pool.max-idle")
				|| environment.containsProperty("spring.redis.jedis.pool.min-idle")
				|| environment.containsProperty("spring.redis.jedis.pool.max-active")
				|| environment.containsProperty("spring.redis.jedis.pool.max-wait");
		ConditionOutcome conditionOutcome = new ConditionOutcome(isSentinel && isJedis, "Enable Redis Sentinel Jedis");
		LOGGER.debug("OnEnableJedisSentinelCondition.getMatchOutcome() conditionOutcome={}", conditionOutcome);
		return conditionOutcome;
	}
}
