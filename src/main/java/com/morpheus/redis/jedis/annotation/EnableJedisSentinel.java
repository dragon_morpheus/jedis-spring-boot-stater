package com.morpheus.redis.jedis.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.morpheus.redis.jedis.sentinel.JedisSentinelImportSelector;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(JedisSentinelImportSelector.class)
public @interface EnableJedisSentinel {
}
