package com.morpheus.redis.jedis.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Import;

import com.morpheus.redis.jedis.sentinel.JedisSentinelImportSelector;
import com.morpheus.redis.jedis.sentinel.OnEnableJedisSentinelCondition;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
@Documented
@Conditional(OnEnableJedisSentinelCondition.class)
@Import(JedisSentinelImportSelector.class)
public @interface ConditionalOnJedisSentinelConfig {
}
