package com.morpheus.redis.jedis.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "spring.redis.jedis")
public class JedisProperties {
	private JMX jmx = null;
	private Test test = null;
	private int connectionTimeout = -1;
	private int soTimeout = -1;
	private int maxAttempts = 3;

	@Data
	public static class JMX {
		private boolean enabled = false;
		private Name name = null;

		@Data
		public static class Name {
			private String base = null;
			private String prefix = null;
		}
	}

	@Data
	public static class Test {
		private boolean onBorrow = true;
		private boolean onCreate = false;
		private boolean onReturn = false;
		private boolean whileIdle = true;
	}
}
